var express = require('express');
var router = express.Router();
var firebase = require('../config');
var db = firebase.firestore()


router.get('/items', function(req, res, next) {
    var thisReturn = {
        type: 'getItems',
        data: null
    }
    
    var allData = []
    db.collection('tci_assets')
    .get()
    .then(snapshot => {
        snapshot.forEach((item)=>{
            allData.push(item.data())
        })
        thisReturn.data = allData
        console.log(thisReturn)
        res.json(thisReturn)
    }).catch((error)=>{
        console.log(error)
    })
});


module.exports = router;