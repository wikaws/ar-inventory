var express = require('express');
var router = express.Router();
var firebase = require('../config');
var db = firebase.firestore()

let items = [];

/* GET home page. */
router.get('/', function(req, res, next) {
  var thisReturn = {
   type: 'getItems',
   data: null
  }

  var allData = []
  db.collection('tci_assets')
  .get()
  .then(snapshot => {
      snapshot.forEach((item)=>{
          allData.push(item.data())
      })
      thisReturn.data = allData
      console.log(thisReturn)
      res.render('index', {data: allData});
  }).catch((error)=>{
      console.log(error)
  })
});

// API route to get all items
router.get('/items', (req, res) => {
  res.render('create');
});

// API route to create a new item
router.post('/items', async (req, res) => {
  var allData = []
  var newData = {
      storage_id: parseInt(req.body.storageId),
      sub_storage: parseInt(req.body.subStorage),
      name: req.body.name,
      total: parseInt(req.body.total)
  };
  await db.collection('tci_assets').add(newData);
  var snapshot = await db.collection ('tci_assets').get();
  snapshot.forEach(item => {
    allData.push(item.data())
  })
  res.render('index', {data: allData, message: 'Data added successfully!'});
});

module.exports = router;
