var createError = require('http-errors');
var express = require('express');
var dgram = require('dgram');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var r = require('rethinkdb');
var cors = require('cors');
const { log } = require('console');

var config = require(__dirname + '/config.js');

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var apiRouter = require('./routes/api');

var app = express();
var udpServer = dgram.createSocket('udp4');

const PORT = 3000;

var clientAddress = {address: '', port: '7979'};

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(logger('dev'));
app.use(express.json());
app.use(cors({
  origin: '*'
}));
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/api', apiRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;